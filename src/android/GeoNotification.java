package com.cowbell.cordova.geofence;

import com.google.android.gms.location.Geofence;
import com.google.gson.annotations.Expose;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import android.util.Log;

public class GeoNotification {
    @Expose public String id;
    @Expose public double latitude;
    @Expose public double longitude;
    @Expose public int radius;
    @Expose public int transitionType;
    @Expose public Date lastAlert;
    @Expose public Notification notification;

    public GeoNotification() {
        this.lastAlert = null;
    }

    public Geofence toGeofence() {
        return new Geofence.Builder()
            .setRequestId(id)
            .setTransitionTypes(transitionType)
            .setCircularRegion(latitude, longitude, radius)
            .setExpirationDuration(Long.MAX_VALUE).build();
    }

    public String toJson() {
        return Gson.get().toJson(this);
    }

    public static GeoNotification fromJson(String json) {
        if (json == null) return null;
        // Logger.setLogger(new Logger(GeofencePlugin.TAG, this, false));
        Logger logger = Logger.getLogger();
        logger.log(Log.DEBUG, json);
        return Gson.get().fromJson(json, GeoNotification.class);
    }

    public boolean HasBeenAlertedRecently(){
        if (this.lastAlert == null){
            Logger logger = Logger.getLogger();
            logger.log(Log.DEBUG, "lastalert is null");
            return false;
        }else {
            // this.logger.log(Log.DEBUG, "notification has been run");
            Date now = new Date();
            long diff = getDateDiff(this.lastAlert, now, TimeUnit.MINUTES);
            if (diff < 60 * 6) {
                return true;
            } else {
                return false;
            }
        }
    }

    public void saveTime(){
        this.lastAlert = new Date();
    }

    private static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }
}
